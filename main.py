import json
import logging
from http.server import HTTPStatus

import httpx
import uvicorn
from fastapi import FastAPI, HTTPException
from fastapi.middleware.cors import CORSMiddleware

from sennder.settings import Settings
from sennder.utils import extract_uuid4

app = FastAPI()
settings = Settings()
origins = ["*"]
app.add_middleware(
    CORSMiddleware, allow_origins=origins, allow_credentials=True, allow_methods=["GET"], allow_headers=["*"],
)
logger = logging.getLogger("api")


@app.get("/")
def read_root():
    return {"home": True}


@app.get("/films/")
def films():
    with open("./films.json", "r") as f:
        return json.load(f)


@app.get("/sync/")
async def sync():
    films_response = []
    people_response = []

    async with httpx.AsyncClient() as client:
        films_endpoint = f"{settings.API_FILMS_BASE_URL}/films"
        response = await client.get(films_endpoint)

        if response.status_code != HTTPStatus.OK:
            msg = f"Could not fetch the endpoint {films_endpoint}"
            logger.error(msg)
            raise HTTPException(
                status_code=HTTPStatus.BAD_REQUEST, detail={"sync": False, "reason": msg},
            )

        films_response = response.json()
        for film in films_response:
            film["people"] = []

    async with httpx.AsyncClient() as client:
        people_endpoint = f"{settings.API_FILMS_BASE_URL}/people"
        response = await client.get(people_endpoint)

        if response.status_code != HTTPStatus.OK:
            msg = f"Could not fetch the endpoint {people_endpoint}"
            logger.error(msg)
            raise HTTPException(
                status_code=HTTPStatus.BAD_REQUEST, detail={"sync": False, "reason": msg},
            )

        people_response = response.json()

    for person in people_response:
        if person["films"]:
            for film_appearance in person["films"]:
                extracted_film_uuid4 = extract_uuid4(film_appearance)

                if not extracted_film_uuid4:
                    logger.warning(f"Film UUID {extracted_film_uuid4} could not be extracted.")
                    continue

                try:
                    film_index = next(
                        (index for (index, film) in enumerate(films_response) if film["id"] == extracted_film_uuid4)
                    )
                except StopIteration:
                    film_index = None
                    logger.warning(f"Film UUID {extracted_film_uuid4} could not be found.")

                if film_index is not None and isinstance(films_response[film_index]["people"], list):
                    films_response[film_index]["people"].append(person)

    with open("./films.json", "w") as f:
        logger.info("Started writing on films.json file")
        json.dump(films_response, f, ensure_ascii=False, indent=4)
        logger.info("Finished writing on films.json file")

    return {"sync": True, "data": {"films": films_response}}


if __name__ == "__main__":
    uvicorn.run(
        "main:app", host=settings.API_SENNDER_HOST, port=settings.API_SENNDER_PORT, reload=settings.API_SENNDER_RELOAD
    )
