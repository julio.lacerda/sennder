# sennder

Represents the backend, there are two major flows here.
* HTTP server
* Data sync (data cache layer)

The backend is responsible for fetching the data from a third-party api, on this case through the [Ghibli](https://ghibliapi.herokuapp.com/) and serving new data to our frontend.
It is important to mention that once the HTTP server is running, we should also start running Data Sync to have the most up to date data from [Ghibli](https://ghibliapi.herokuapp.com/) 

## via docker

Create an empty json file
```bash
cp films.example.json films.json
```

### http server
 
#### build

```bash
docker build -t sennder .
```

#### run

```bash
docker run -it -v ${PWD}:/code -p 8000:8000 sennder
```

### data sync

#### build
```bash
docker build -f SyncDockerfile -t sennder-sync . 
```

#### run
```bash
docker run -it -v ${PWD}:/code  sennder-sync
```

## via localhost

###### OS dependencies

* Install Python 3.6.*
* Install [Poetry](https://python-poetry.org/docs/#installation)

###### Python dependencies

```bash
poetry install
```

Create an empty json file
```bash
cp films.example.json films.json
```

### run api

```bash
poetry shell
python main.py
```

### run sync

```bash
poetry shell
python sync.py
```
