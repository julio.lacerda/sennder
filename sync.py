import logging
import signal
import threading
import time
from datetime import timedelta

import httpx

from sennder.settings import Settings

settings = Settings()
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("sync")


def sync_films():
    try:
        response = httpx.get(settings.API_SENNDER_SYNC_URL)
        logger.info(f"Last synced at {time.ctime()} with response code {response.status_code}")
    except httpx.ConnectError as e:
        logger.error(f"Failed at {time.ctime()} caused by connection error {e}")
    except Exception as e:
        logger.error(f"Failed at {time.ctime()} caused by {e}")


class ProgramKilled(Exception):
    pass


def signal_handler(signum, frame):
    raise ProgramKilled


class Job(threading.Thread):
    def __init__(self, interval, execute, *args, **kwargs):
        threading.Thread.__init__(self)
        self.daemon = False
        self.stopped = threading.Event()
        self.interval = interval
        self.execute = execute
        self.args = args
        self.kwargs = kwargs

    def stop(self):
        self.stopped.set()
        self.join()

    def run(self):
        while not self.stopped.wait(self.interval.total_seconds()):
            self.execute(*self.args, **self.kwargs)


if __name__ == "__main__":
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(signal.SIGINT, signal_handler)
    job = Job(interval=timedelta(milliseconds=settings.API_SENNDER_WAIT_TIME_MILLISECONDS), execute=sync_films)
    job.start()

    logger.info("Started sennder-sync")
    while True:
        try:
            time.sleep(1)
        except ProgramKilled:
            print("Program killed: running cleanup code")
            job.stop()
            logger.info("Stopped sennder-sync")
            break
