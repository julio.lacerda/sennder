from pydantic import BaseSettings


class Settings(BaseSettings):
    API_FILMS_BASE_URL = "https://ghibliapi.herokuapp.com"
    API_SENNDER_WAIT_TIME_MILLISECONDS = 1000 * 60
    API_SENNDER_HOST = "0.0.0.0"
    API_SENNDER_PORT = 8000
    API_SENNDER_RELOAD = True
    API_SENNDER_SYNC_URL = f"http://{API_SENNDER_HOST}:{API_SENNDER_PORT}/sync/"
