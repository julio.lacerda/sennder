import re
import uuid


def validate_uuid4(uuid_string):
    try:
        uuid.UUID(uuid_string, version=4)
    except ValueError:
        # If it's a value error, then the string
        # is not a valid hex code for a UUID.
        return False

    return True


def extract_uuid4(string):
    pattern = re.compile("^.*([0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12})$")
    match = pattern.match(string)
    if match and validate_uuid4(match.groups()[0]):
        return match.groups()[0]
    return None
